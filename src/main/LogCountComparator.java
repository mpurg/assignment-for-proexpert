package main;

import java.util.Comparator;

import model.LogCount;

public class LogCountComparator implements Comparator<LogCount>{

	@Override
	public int compare(LogCount o1, LogCount o2) {
		return (int)(o2.getTime() - o1.getTime());
	}

}
