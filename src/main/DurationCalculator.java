package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import model.LogCount;
import model.LogLine;

public class DurationCalculator {
	private List<LogCount> countedLogs;
	private List<LogLine> unsortedList;
	public DurationCalculator(List<LogLine> logs){
		unsortedList = logs;
	}
	
	public List<LogCount> showSlowestRequests(int topCount){
		countLogs();
		List<LogCount> sortedList = sortCountLogs();
		if(topCount > sortedList.size()){
			return sortedList.subList(0, sortedList.size());
		}
		return sortCountLogs().subList(0, topCount);
	}
	
	private void countLogs(){
		countedLogs = new ArrayList<LogCount>();
		for (LogLine line : unsortedList) {
			boolean calculated = false;
			for(int i = 0; i < countedLogs.size(); i++){
				if(countedLogs.get(i).getId().equals(line.getId())){
					countedLogs.get(i).increaseTime(line.getTime());
					calculated = true;
					break;
				}
			}
			if(!calculated){
				countedLogs.add(new LogCount(line.getId(), line.getTime()));
			}
		}
	}
	
	private List<LogCount> sortCountLogs(){
		
		findAvarageOfEachLog();
		Collections.sort(countedLogs, new LogCountComparator());

		return countedLogs;
	}
	
	private void findAvarageOfEachLog(){
		for(int i = 0; i < countedLogs.size();i++){
			long time = countedLogs.get(i).getTime();
			int count = countedLogs.get(i).getCount();
			int avarage = (int)(time/count);
			countedLogs.get(i).setTime(avarage);
			countedLogs.get(i).setCount(1);

		}
	}
}
