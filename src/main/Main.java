package main;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import model.LogCount;
import model.LogLine;
import util.FileUtil;

public class Main {

	public static void main(String[] args) {
		
		long startTime = System.currentTimeMillis();
		if(args.length > 0 && args[0].equals("-h")){
			System.out.println("This program displays different info from a log file.");
			System.out.println("It requires  2 arguments.");
			System.out.println("First for the log file path(log.txt, timing.log, etc), the second for the number of displayed answers(5, 10, etc ).");
			return;
		}
		if (args.length != 2) {
			System.out.println("The program requires  2 parameters.");
			System.out.println("First for the log file path(log.txt), the second for the number of displayed answers.");
			return;
		}
		
		if (!inputCheck(args[0], args[1])) return;
		
		List<String> textLines = FileUtil.readFromFile(args[0]);
		List<LogLine> data = parseText(textLines);
		DurationCalculator calculator = new DurationCalculator(data);
		
		int count = Integer.parseInt(args[1]);
		if(count > 0){
			System.out.println("The avarage slowest " + args[1] + " requests are as followed:");
			for (LogCount log : calculator.showSlowestRequests(count)) {
				System.out.println(log.getId() + ", " + log.getTime() + "ms");
			}
		}
		
		System.out.println("Hourly histogram:");
		Histogram.showHistogram(data);
		
		long stopTime = System.currentTimeMillis();
		long duration = stopTime - startTime;
		System.out.println("Program took " + duration + "ms");
		
	}

	private static boolean inputCheck(String filePath, String pageNumber) {
		boolean fileExists = new File("./" + filePath).exists();
		if (!fileExists) {
			System.out.println("There was a problem loading the file " + filePath + ", or the path does not exist.");
		}
		boolean pageIsNumber = tryParse(pageNumber);
		if (!pageIsNumber) {
			System.out.println("Your second parameter " + pageNumber + " should be an integer."
					+ " It indicates the number of results.");
		}
		return fileExists && pageIsNumber;
	}

	private static boolean tryParse(String number) {
		try {
			Integer.parseInt(number);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private static List<LogLine> parseText(List<String> log) {
		List<LogLine> result = new ArrayList<LogLine>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
		for (String line : log) {
			if(line.equals("")){
				continue;
			}
			String date = line.substring(0,23);
			Calendar formattedDate = Calendar.getInstance();
			try {
				formattedDate.setTime(format.parse(date));
			} catch (java.text.ParseException e) {
				System.out.println("Date is in the wrong format");
				return null;
			}
			
			int fromIndex = line.indexOf('[') + 1;
			int toIndex = line.indexOf(']') ;
			String formattedID = line.substring(fromIndex, toIndex);
			
			int formattedTime = 0;
			fromIndex = line.indexOf(" in ");
			String numberString = line.substring(fromIndex + 4);
			if (!tryParse(numberString)) {
				System.out.println("request duration is not represented correctly.");
				return null;
			} else {
				formattedTime = Integer.parseInt(numberString);
			}
			
			result.add(new LogLine(formattedDate, formattedTime, formattedID));
		}
		return result;
	}
}
