package main;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import model.LogLine;

public class Histogram {
	private static final int STARLIMIT = 13;
	
	public static void showHistogram(List<LogLine> logLines){
		Calendar min = logLines.get(0).getDate();
		Calendar max = logLines.get(0).getDate();
		for (LogLine logLine : logLines) {
			if(min.getTime().after(logLine.getDate().getTime())){
				min = logLine.getDate();
			}
			if(max.getTime().before(logLine.getDate().getTime())){
				max = logLine.getDate();
			}
		}
		int hours = calculateCalendarDifferenceInHours(max, min) + 1;
		int[] count = createEmptyCount(hours);
		for (int i = 0; i < logLines.size(); i++) {
			int hourDifference = calculateCalendarDifferenceInHours(logLines.get(i).getDate(), min);
			count[hourDifference]++;
		}
		displayHistogram(count, min);
	}
	
	private static int calculateCalendarDifferenceInHours(Calendar first, Calendar second){
		return (int)(first.getTimeInMillis() - second.getTimeInMillis()) / (1000 * 60 * 60);
	}
	
	private static int[] createEmptyCount(int size){
		int [] array = new int[size];
		for (int i = 0; i < array.length; i++) {
			array[i] = 0;
		}
		return array;
	}
	
	private static void displayHistogram(int[] histogramArray, Calendar earliestDate){
		SimpleDateFormat format = new SimpleDateFormat("MM-dd HH");
		int biggest = histogramArray[0];
		for (int item : histogramArray) {
			if(biggest < item){
				biggest = item;
			}
		}
		double eachStar = (double)(STARLIMIT - 1) / (double)biggest;
		for (int i = 0; i < histogramArray.length; i++) {
			if(histogramArray[i] != 0){
				System.out.println(format.format(earliestDate.getTime()) + ": " + showStars(histogramArray[i], eachStar));
			}
			earliestDate.add(Calendar.HOUR_OF_DAY, 1);
		}
	}
	
	private static String showStars(int count, double starLimit){
		StringBuilder sb = new StringBuilder();
		int stars = (int) (count * starLimit) + 1;
		for (int i = 0; i < stars; i++) {
			sb.append("*");
		}
		return sb.toString();
	}
}
