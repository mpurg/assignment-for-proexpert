package model;

import java.util.Calendar;

public class LogLine {
	private Calendar date;
	private int time;
	private String uniqueId;
	
	public LogLine(Calendar date, int time, String uniqueID){
		this.date = date;
		this.time = time;
		this.uniqueId = uniqueID;
	}
	
	public LogLine( int time, String uniqueID){
		this.time = time;
		this.uniqueId = uniqueID;
	}
	
	public Calendar getDate(){
		return date;
	}
	
	public int getTime(){
		return time;
	}
	
	public String getId(){
		return uniqueId;
	}
}
