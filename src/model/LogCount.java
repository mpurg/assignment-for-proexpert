package model;

public class LogCount {
	private long time;
	private int count;
	private String uniqueId;
	
	public LogCount(String id, int time){
		this.uniqueId = id;
		this.count = 1;
		this.time = time;
	}
	
	public void increaseTime(int time){
		this.time += time;
		this.count++;
	}
	
	public long getTime(){
		return time;
	}
	
	public void setTime(int time){
		this.time = time;
	}
	
	public int getCount(){
		return count;
	}
	
	public void setCount(int count){
		this.count = count;
	}
	
	public String getId(){
		return uniqueId;
	}
}
