package util;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {

	public static List<String> readFromFile(String path) {
		List<String> result = new ArrayList<String>();
		try (FileInputStream is = new FileInputStream("./" + path);
				BufferedReader r = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {

			String str = null;
			while ((str = r.readLine()) != null) {
				result.add(str);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return result;
	}
}
